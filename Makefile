CC := gcc
CFLAGS := -g0 -O3 -Wall -Werror -Wextra
LIBS := -lX11 -lXext -lXtst

all:
	${CC} ${CFLAGS} inusb1.c -o inusb1 ${LIBS}
	${CC} ${CFLAGS} inusb1-learn.c -o inusb1-learn ${LIBS}
dist:
	${CC} -mtune=generic ${CFLAGS} inusb1.c -o inusb1 ${LIBS}
	${CC} -mtune=generic ${CFLAGS} inusb1-learn.c -o inusb1-learn ${LIBS}

