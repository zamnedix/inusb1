#pragma once

/* Change below definitions for key mappings */
#define REWIND_CODE XK_Shift_L
#define PLAY_CODE XK_Control_L
#define FORWARD_CODE XK_Tab
/* ***************************************** */

//Change if your device uses a different packet size
#define PACKETSZ 24

typedef struct command command;
typedef struct command_set command_set;

struct command {
  uint8_t data[PACKETSZ];
} __attribute__((packed));

struct command_set {
  command rew;
  command play;
  command fwd;
  command rst;
} __attribute__((packed));
