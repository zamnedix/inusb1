#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "inusb1.h"

int hid_fd;
int out_fd;
command null_command;
command_set commands = {0};

int learn_command(char* text, command* cmd) {
  printf("%s", text);
  if (read(hid_fd, cmd, PACKETSZ) != PACKETSZ) {
    fprintf(stderr, "[error] learn_command: failed to read from HID device: %s\n", strerror(errno));
    return 1;
  }
  return 0;
}

int learn_commands() {
  if (learn_command("*PRESS* REWIND:\n", &commands.rew) ||
      learn_command("*RELEASE* REWIND:\n", &commands.rst) ||
      learn_command("*PRESS* PLAY:\n", &commands.play) ||
      learn_command("*RELEASE* PLAY:\n", &null_command) ||
      learn_command("*PRESS* FWD:\n", &commands.fwd) ||
      learn_command("*RELEASE* FWD:\n", &null_command)) {
    fprintf(stderr, "[error] learn_commands: failed to read a command\n");
    return 1;
  }
  return 0;
}

int write_commands() {
  if (write(out_fd, &commands, sizeof(command_set)) != sizeof(command_set)) {
    fprintf(stderr, "[error] write_commands: failed to write command set: %s\n", strerror(errno));
    return 1;
  }
  if (close(out_fd) == -1) {
    fprintf(stderr, "[error] write_commands: failed to close file descriptor for output data: %s\n", strerror(errno));
    return 1;
  }
  return 0;
}

int main(int argc, char** argv) {
  if (argc < 3) {
    fprintf(stderr, "[fatal] main: not enough arguments\n"
                    "Syntax: %s <HID device path> <output data path>\n", argv[0]);
    return 127;
  }
  if ((hid_fd = open(argv[1], O_RDONLY)) == -1) {
    fprintf(stderr, "[fatal] main: couldn't open HID device at \"%s\": %s\n", argv[1], strerror(errno));
    return 1;
  }
  if ((out_fd = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC)) == -1) {
    fprintf(stderr, "[fatal] main: couldn't open output file at \"%s\": %s\n", argv[2], strerror(errno));
    return 2;
  }
  if (learn_commands()) {
    fprintf(stderr, "[fatal] main: error while learning commands\n");
    return 3;
  }
  if (write_commands()) {
    fprintf(stderr, "[fatal] main: error while writing command set to output file\n");
    return 4;
  }
  fprintf(stderr, "[info] main: exiting successfully\n");
  return 0;
}

