#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <stdbool.h>
#include <sched.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <X11/extensions/XTest.h>
#include "inusb1.h"

char* devpath, * datapath;
int device;
uint8_t packet_buf[PACKETSZ];
Display* display;
KeyCode key = 0;
bool mode = true;
command_set commands;

/*const uint8_t const play_pressed[] =
  {0x01, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x02, 0x00, 0x09, 0x00, 0x01, 0x00, 0x00, 0x00,
   0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t const rewind_pressed[] =
  {0x01, 0x00, 0x09, 0x00, 0x01, 0x00, 0x00, 0x00,
   0x02, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t const forward_pressed[] =
  {0x01, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x02, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x03, 0x00, 0x09, 0x00, 0x01, 0x00, 0x00, 0x00};
const uint8_t const reset[] =
  {0x01, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x02, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00};*/

static int read_commands() {
  int fd;

  if ((fd = open(datapath, O_RDONLY)) == -1) {
    fprintf(stderr, "[fatal] read_commands: couldn't open command set at \"%s\": %s\n",
            datapath, strerror(errno));
    return 1;
  }
  if (read(fd, &commands, sizeof(command_set)) != sizeof(command_set)) {
    fprintf(stderr, "[fatal] read_commands: couldn't read from command set: %s\n",
            strerror(errno));
    return 1;
  }
  if (close(fd) == -1) {
    fprintf(stderr, "[warn] read_commands: couldn't close file descriptor of command set: %s\n",
            strerror(errno));
  }
  return 0;
}

static int open_device() {
  if ((device = open(devpath, O_RDONLY|O_NONBLOCK)) == -1) {
    fprintf(stderr, "[fatal] open_device: couldn't open HID device at \"%s\": %s\n",
            devpath, strerror(errno));
    return 1;
  }
  if ((fcntl(device, F_SETFL, O_ASYNC) == -1) ||
      (fcntl(device, F_SETOWN, getpid()) == -1)) {
    fprintf(stderr, "[fatal] open_device: couldn't figure asynchronous input: %s\n", strerror(errno));
    return 1;
  }
  return 0;
}

static int x_open() {
  if (!(display = XOpenDisplay(0))) {
    fprintf(stderr, "[fatal]: x_open: couldn't open display 0\n");
    return 1;
  }
  return 0;
}

static int reset() {
	mode = false;
	key = XKeysymToKeycode(display, REWIND_CODE);
	XTestFakeKeyEvent(display, key, mode, 0);
	XFlush(display);
	
	key = XKeysymToKeycode(display, PLAY_CODE);
	XTestFakeKeyEvent(display, key, mode, 0);
	XFlush(display);
	
	key = XKeysymToKeycode(display, FORWARD_CODE);
	XTestFakeKeyEvent(display, key, mode, 0);
	XFlush(display);
	
	if (close(device) == -1) {
		return 1;
	}
/*	XCloseDisplay(display);
	
	if (x_open()) {
		return 1;
		}*/
	if (open_device()) {
		return 1;
	}
	return 0;
}

static void process_event() {
  if (!(memcmp(packet_buf, &commands.rew, PACKETSZ))) {
    key = XKeysymToKeycode(display, REWIND_CODE);
    mode = true;
  }
  else if (!(memcmp(packet_buf, &commands.play, PACKETSZ))) {
    key = XKeysymToKeycode(display, PLAY_CODE);
    mode = true;
  }
  else if (!(memcmp(packet_buf, &commands.fwd, PACKETSZ))) {
    key = XKeysymToKeycode(display, FORWARD_CODE);
    mode = true;
  }
  else if (!(memcmp(packet_buf, &commands.rst, PACKETSZ))) {
    mode = false;
  }
  else {
    fprintf(stderr, "[error]: process_event: unrecognized packet, reset state\n");
    if (reset()) {
	    fprintf(stderr, "[error]: process_event: failed to reset, exiting\n");
	    exit(1);
    }
    return;
  }
  XTestFakeKeyEvent(display, key, mode, 0);
  XFlush(display);
}

static void trigger() {
  int ret;
  if ((ret = read(device, packet_buf, PACKETSZ)) == -1) {
    fprintf(stderr, "[error] trigger: read failed: %s\n", strerror(errno));
    return;
  }
  process_event();
}

int main(int argc, char** argv) {
  if (argc < 3) {
    fprintf(stderr, "[fatal] main: not enough arguments\n"
                    "Syntax is: %s <path to USB HID device> <path to command set file>\n", argv[0]);
    return 127;
  }
  devpath = argv[1];
  datapath = argv[2];
  if (read_commands()) {
    return 1;
  }
  if (x_open()) {
    return 2;
  }
  if (signal(SIGIO, trigger) == SIG_ERR) {
    return 3;
  }
  if (open_device()) {
    return 4;
  }
  while (1) {
    pause();
  }
  return 0;
}

