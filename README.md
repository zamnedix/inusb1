I designed this tool to work with the Infinity IN-USB-1 footpedal. Mine shows up as a "VEC USB Footpedal" in dmesg ("PI Engineering, Inc. VEC Footpedal" in lsusb.)  
Example usage:  
$ sudo ./inusb1 /dev/usb/hiddev1 inusb1.dat  

I have provided a sample udev rule file ("100-inusb1.rules") so you can run it as a normal user. This is safer, but not required.
To use it, change the username from {OWNER="zamnedix"} to your own, and install it to /etc/udev/rules.d. Finally, if it's already plugged in,
manually trigger an add event like so:  
$ sudo udevadm trigger -c add /dev/usb/hiddev1  

Right now, REWIND is mapped to left shift, PLAY to left ctrl, and FORWARD to tab. You can change those
in the first few #defines of inusb1.h. The keysyms are here: https://www.cl.cam.ac.uk/~mgk25/ucs/keysymdef.h  
If it doesn't seem to work, you can try learning a new command set with inusb1-learn:  
$ sudo ./inusb1-learn /dev/usb/hiddev1 new.dat  
$ sudo ./inusb1 /dev/usb/hiddev1 new.dat  


